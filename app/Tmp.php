<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tmp extends Model
{
    //
    protected $table = 'tmps';
    protected $fillable = [
        'img_path', 'user_token'
    ];

    public function getAllTmpAttachedTotoken($token){
        $arrTmp = $this->WhereIn('user_token',$token)->get();
        // dd($arrTmp);
        return $arrTmp;
    } 
    public function deleteAllTmp($Ids){
        // dd($Ids);
        $result = $this->WhereIn('user_token',$Ids)->toSql();
        // dd($result);
        return $result;
    }
}
