<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tmp;
use Illuminate\Support\Facades\Auth;
use App\Img;
use Illuminate\Support\Facades\File;
class UserController extends Controller
{
    //

    public function Register(Request $request){
        
        $objUser = new User();
        // $objUser->name = $request->name;
        // $objUser->email = $request->email;
        // $objUser->password = Bcrypt($request->password) ;
        // $result = $objUser->save();
        // $proxy = Request::create(
        //     'oauth/token',
        //     'POST'
        // );
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
 
        $token = $user->createToken('token')->accessToken;
        return response()->json(['token' => $token], 200);
        return $result;
    }


    public function Login(Request $request){
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('token')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    public function Upload(Request $request){
        $data = array();
        $image = $request->file('file');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        // dd($destinationPath);
        $image->move($destinationPath, $name);
        $obj = new Tmp();
        $obj->user_token = Auth::user()->id;
        $obj->img_path = $destinationPath.$name;
        $result = $obj->save();
        // $this->save();
        if($result){
            $data['user'] = Auth::user();
            $data['result'] = "success";
        }else{
            $data['result'] = "failed";
        }
        
        return $data;
        
    }
    public function SubmitForm(Request $request){
        //  dd($request);
        $arrIds = array();
        $obj = new Tmp();
        $arrTmp = $obj->getAllTmpAttachedTotoken(Auth::user());
        foreach($arrTmp as $object){
            $objImg = new Img();
            $arrIds[] = $object['id'];
            $objImg->img = $object['img_path'];
            $objImg->save();

        }
        $result = $obj->deleteAllTmp($arrIds);
        // dd($result);
        if($result){
            $data['user'] = Auth::user();
            $data['result'] = "success";
        }else{
            $data['result'] = "failed";
        }
        
    }


    public function DeleteFile(Request $request){
        $image_path = public_path('/images/1572344451.png');  // Value is not URL but directory file path
        // $result = File::delete($image_path);
        // dd($result);
        // return $result;
        if(File::exists($image_path)) {
            $result = File::delete($image_path);
            return $result;
        }
        $result = 'false';
        return $result;
    }
}
