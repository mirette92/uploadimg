<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/create', 'UserController@Register');
Route::post('/login', 'UserController@Login');

Route::middleware('auth:api')->post('/upload', 'UserController@Upload');
Route::middleware('auth:api')->post('/submitForm', 'UserController@SubmitForm');

Route::post('/deleteImg', 'UserController@DeleteFile');